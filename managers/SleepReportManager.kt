package com.wondercise.wondercisetimelessband.managers

import android.annotation.SuppressLint
import android.content.ContentValues
import android.util.Log
import com.android.ble.oad.Util
import com.wondercise.wondercisetimelessband.TimelessBandBleAlgorithmHelper
import com.wondercise.wondercisetimelessband.database.DatabaseHandler
import com.wondercise.wondercisetimelessband.sContext
import com.wondercise.wondercisetimelessband.util.SQliteOpenHelper_Sleep
import com.wondercise.wondercisetimelessband.util.SleepRecord
import com.wondercise.wondercisetimelessband.util.Spo2Record
import io.reactivex.Single
import io.reactivex.SingleEmitter
import java.lang.Exception
import java.util.*

private const val TAG = "SleepReportManager"

class SleepReportManager(
    private val macAddress: String,
    private val timelessBandBleAlgorithmHelper: TimelessBandBleAlgorithmHelper
    )
{

    inner class SleepReportBean (
        var beginTime: Long,
        var duration: Int,
        var deepSleepDuration: Int,
        var lightSleepDuration: Int,
        var remSleepDuration: Int,
        var awakeSleepDuration: Int,
        var curveListNum: Int,
        var bodyMoveNum: Int,
        var spo2Num: Int,
        var sleepCurve: MutableList<SleepCurveBean>,
        var heartCurve: MutableList<Int>,
        var bodyMoveCurve: MutableList<Int>,
        var spo2CurveMap: TreeMap<Long, Int>,
        var score: Int,
    )

    class SleepCurveBean(var type: Int, var duration: Int) {
        override fun toString(): String {
            return "$type, $duration"
        }
    }

    private lateinit var mSleepReportBeanSingleEmitter: SingleEmitter<MutableList<SleepReportBean>>

    private val dbHelper_sleep = SQliteOpenHelper_Sleep(sContext, macAddress)
    private val contentValues_sleep = ContentValues()

    private val mSleepReportBeanList = mutableListOf<SleepReportBean>()
    private var mSleepReportBeanData: SleepReportBean ?= null

    private var retryTime = 0
    private var maxRetryTimes = 3

    private var mSleepReport: List<SleepRecord> = listOf()
    private lateinit var mSleepReportSingleEmitter: SingleEmitter<List<SleepRecord>>

    init {
        Log.d("SleepReportManager", "init")
    }

    fun reset() {
        retryTime = 0
        mSleepReportBeanList.clear()

        mSleepReportBeanData = SleepReportBean(
            0L,0,0,0,0,0,0,
            0,0, mutableListOf(), mutableListOf(), mutableListOf(),TreeMap<Long, Int>(),0
        )
    }

    fun resetList() {
        mSleepReport = listOf()
    }

    fun deleteDB() {
        val sqliteDatabase = dbHelper_sleep.writableDatabase
        sqliteDatabase.delete("timelessSleep_sql", null, null)
    }

    fun setCallback(call: SingleEmitter<MutableList<SleepReportManager.SleepReportBean>>) {
        mSleepReportBeanSingleEmitter = call
    }

    fun setSleepReportCallback(call: SingleEmitter<List<SleepRecord>>) {
        mSleepReportSingleEmitter = call
    }

    fun onReceivedSleepData(data: ByteArray) {
        val operationCode: Int = data[0].toInt().and(0xff)
        when (operationCode) {
            0x01 -> {
                //睡眠总条数
                val reportCount = data[1].toInt().and(0xff)
                Log.d(TAG,"Sleep report Count：$reportCount")

                if (reportCount == 0) {
                    mSleepReportBeanSingleEmitter?.onSuccess(mSleepReportBeanList)
                } else {
                    timelessBandBleAlgorithmHelper.sendCommand2(byteArrayOf(0x1B, 0x02))
                }
            }
            0x02 -> {
                //睡眠报告数据
                val pkgId: Int = data[1].toInt().and(0xff)
                if (pkgId == 0x01) {
                    mSleepReportBeanData?.beginTime = Util.buildUint32(data.sliceArray(2..5))
                    mSleepReportBeanData?.duration = Util.buildUint16(data[6], data[7])
                    mSleepReportBeanData?.deepSleepDuration = Util.buildUint16(data[8], data[9])
                    mSleepReportBeanData?.lightSleepDuration  = Util.buildUint16(data[10], data[11])
                    mSleepReportBeanData?.remSleepDuration = Util.buildUint16(data[12], data[13])
                    mSleepReportBeanData?.awakeSleepDuration = Util.buildUint16(data[14], data[15])
                    mSleepReportBeanData?.score = data[16].toInt().and(0xff)
                } else if (pkgId == 0x02) {
                    mSleepReportBeanData?.curveListNum = Util.buildUint16(data[2], data[3])//區間列表總條數
                    mSleepReportBeanData?.bodyMoveNum = Util.buildUint16(data[4], data[5])//心率體動條數
                    mSleepReportBeanData?.spo2Num = Util.buildUint16(data[6], data[7])//血氧數據條數
                    Log.d(TAG,"收到睡眠區間總數:${mSleepReportBeanData?.curveListNum} ")
                    Log.d(TAG,"收到睡眠體動数据总条数:${mSleepReportBeanData?.bodyMoveNum} ")
                    Log.d(TAG,"收到睡眠血氧数据总条数:${mSleepReportBeanData?.spo2Num} 条")
                    mSleepReportBeanData?.spo2CurveMap?.clear()
                    if (mSleepReportBeanData?.curveListNum != 0) {
                        Log.d(TAG,"睡眠报告头获取完成，开始获取睡眠区间过程数据 数据条数：${mSleepReportBeanData?.curveListNum}")
                        timelessBandBleAlgorithmHelper.sendCommand2(byteArrayOf(0x1B, 0x03))
                    } else {
                        //callbackReport.onGetReport(bean)
                    }
                }
            }
            0x03 -> {
                //睡眠过程数据
                val totalCount = data[1].toInt().and(0xff)//總包數
                val curPkg = data[2].toInt().and(0xff)//包序
                val pkgDataNum = data[3].toInt().and(0xff)//此包條數
                for (index in 0 until pkgDataNum) {
                    val beginIndex = index * 3 + 4
                    val type = data[beginIndex].toInt().and(0xff)
                    val duration = Util.buildUint16(data[beginIndex + 1], data[beginIndex + 2])
                    mSleepReportBeanData?.sleepCurve?.add(SleepCurveBean(type, duration))
                }
                if (totalCount == curPkg && mSleepReportBeanData?.sleepCurve?.size == mSleepReportBeanData?.curveListNum) {
                    //收完了过程数据
                    Log.d(TAG,"睡眠区间过程数据获取完成，开始获取睡眠心率、体动过程数据 数据条数：${mSleepReportBeanData?.bodyMoveNum}")
                    timelessBandBleAlgorithmHelper.sendCommand2(byteArrayOf(0x1B, 0x04))
                    // ToDo: need to make sure the continuity of records
                } else if (totalCount == curPkg) {
                    //丢包了数据对不上
                    Log.d(
                        TAG,"睡眠区间过程数据发送丢包，重试获取 丢包条数：${mSleepReportBeanData?.curveListNum?.minus(
                        mSleepReportBeanData?.sleepCurve?.size!!
                    )}")
                    mSleepReportBeanData?.sleepCurve?.clear()
                    if (retryTime++ >= maxRetryTimes) {
                        Log.d(TAG,"丢包次数过多，自动退出同步")
                        mSleepReportBeanSingleEmitter?.onError(IllegalStateException("丢包次数过多，自动退出同步"))
                        return
                    }
                    timelessBandBleAlgorithmHelper.sendCommand2(byteArrayOf(0x1B, 0x03))
                }
            }
            0x04 -> {
                //睡眠心率、体动数据
                val curPkg = data[1].toInt().and(0xff)//包序
                val pkgDataNum = data[2].toInt().and(0xff)//本包條數
                for (index in 0 until pkgDataNum) {
                    val beginIndex = index * 2 + 3
                    val heartRate = data[beginIndex].toInt().and(0xff)
                    val bodyMove = data[beginIndex + 1].toInt().and(0xff)
                    mSleepReportBeanData?.bodyMoveCurve?.add(bodyMove)
                    mSleepReportBeanData?.heartCurve?.add(heartRate)
                }
                // ToDo: need to make sure the continuity of records
                if (mSleepReportBeanData?.bodyMoveCurve?.size == mSleepReportBeanData?.bodyMoveNum) {
                    //收完了过程数据
                    Log.d(TAG,"睡眠心率体动数据获取完成，开始获取睡眠血氧数据")

                    if (mSleepReportBeanData?.spo2Num != 0) {
                        timelessBandBleAlgorithmHelper.sendCommand2(byteArrayOf(0x1B, 0x06))
                    } else {
                        insertData2SleepSQlite()
                        timelessBandBleAlgorithmHelper.sendCommand2(byteArrayOf(0x1B, 0x05))
                    }
                } else if (pkgDataNum != 8) {
                    //丢包了数据对不上
                    Log.d(
                        TAG,"睡眠心率体动数据发送丢包，重试获取 丢包条数：${mSleepReportBeanData?.bodyMoveNum?.minus(
                        mSleepReportBeanData?.bodyMoveCurve?.size!!
                    )}")
                    mSleepReportBeanData?.bodyMoveCurve?.clear()
                    mSleepReportBeanData?.heartCurve?.clear()
                    if (retryTime++ >= maxRetryTimes) {
                        Log.d(TAG,"丢包次数过多，自动退出同步")
                        mSleepReportBeanSingleEmitter?.onError(IllegalStateException("丢包次数过多，自动退出同步"))
                        return
                    }
                    timelessBandBleAlgorithmHelper.sendCommand2(byteArrayOf(0x1B, 0x04))
                }
            }
            0x06 -> {
                //睡眠血氧数据
                val curPkgNum = data[1].toInt().and(0xff)
                for (index in 0 until curPkgNum) {
                    val beginIndex = index * 5 + 2
                    val dateTime = Util.buildUint32(data.sliceArray(beginIndex..beginIndex + 3))
                    val spo2 = data[beginIndex + 4].toInt().and(0xff)
                    mSleepReportBeanData?.spo2CurveMap?.put(dateTime, spo2)
                }
                if (mSleepReportBeanData?.spo2Num == mSleepReportBeanData?.spo2CurveMap?.size) {
                    //血氧完整接受。下一步通知睡眠取完
                    Log.d(TAG,"睡眠血氧数据获取完成")
                    insertData2SleepSQlite()
                    Log.d(TAG,"準備請除睡眠血氧数据")
                    timelessBandBleAlgorithmHelper.sendCommand2(byteArrayOf(0x1B, 0x05))
                } else if (curPkgNum != 3) {
                    //丢包了数据对不上
                    Log.d(
                        TAG,"睡眠血氧发送丢包，重试获取 丢包条数：${mSleepReportBeanData?.spo2Num?.minus(
                        mSleepReportBeanData?.spo2CurveMap?.size!!
                    )}")
                    mSleepReportBeanData?.spo2CurveMap?.clear()
                    if (retryTime++ >= maxRetryTimes) {
                        Log.d(TAG,"丢包次数过多，自动退出同步")
                        mSleepReportBeanSingleEmitter?.onError(IllegalStateException("丢包次数过多，自动退出同步"))
                        return
                    }
                    timelessBandBleAlgorithmHelper.sendCommand2(byteArrayOf(0x1B, 0x06))
                }
            }
            0x05 -> {
                //睡眠删除确认
                Log.d(TAG,"设备回复报告删除完成")
                //同步完資料 在看筆數  如果不為0代表還有資料
                timelessBandBleAlgorithmHelper.sendCommand2(byteArrayOf(0x1B, 0x01))
            }
        }
    }

    private fun insertData2SleepSQlite() {
        Log.d(TAG, "insertData2 Sleep SQlite +++")
        val sqliteDatabase1 = dbHelper_sleep.writableDatabase
        mSleepReportBeanData?.let {
            contentValues_sleep.put("beginTime", it.beginTime)
            contentValues_sleep.put("duration", it.duration)
            contentValues_sleep.put("deepSleepDuration", it.deepSleepDuration)
            contentValues_sleep.put("lightSleepDuration", it.lightSleepDuration)
            contentValues_sleep.put("remSleepDuration", it.remSleepDuration)
            contentValues_sleep.put("awakeSleepDuration", it.awakeSleepDuration)
            contentValues_sleep.put("curveListNum", it.curveListNum)
            contentValues_sleep.put("bodyMoveNum", it.bodyMoveNum)
            contentValues_sleep.put("spo2Num", it.spo2Num)
            contentValues_sleep.put("sleepCurve", it.sleepCurve.toString())
            contentValues_sleep.put("heartCurve", it.heartCurve.toString())
            contentValues_sleep.put("bodyMoveCurve", it.bodyMoveCurve.toString())
            contentValues_sleep.put("spo2CurveMap", it.spo2CurveMap.toString())
            contentValues_sleep.put("score", it.score)
        }
        sqliteDatabase1.insert("timelessSleep_sql",null, contentValues_sleep)
        sqliteDatabase1.close()
    }

    @SuppressLint("Range")
    private fun parseDB2SleepBean(range: DatabaseHandler.DateRange? = null) {
        val sqliteDatabase = dbHelper_sleep.writableDatabase
        val cursor = sqliteDatabase.rawQuery("select * from timelessSleep_sql", null)
        cursor.moveToFirst()
        if (cursor.count != 0) {  //count 指筆數
            for (i in 1..cursor.count) {
                mSleepReportBeanData?.beginTime = cursor.getLong(cursor.getColumnIndex("beginTime"))
                mSleepReportBeanData?.duration = cursor.getInt(cursor.getColumnIndex("duration"))
                mSleepReportBeanData?.deepSleepDuration = cursor.getInt(cursor.getColumnIndex("deepSleepDuration"))
                mSleepReportBeanData?.lightSleepDuration = cursor.getInt(cursor.getColumnIndex("lightSleepDuration"))
                mSleepReportBeanData?.remSleepDuration = cursor.getInt(cursor.getColumnIndex("remSleepDuration"))
                mSleepReportBeanData?.awakeSleepDuration = cursor.getInt(cursor.getColumnIndex("awakeSleepDuration"))
                mSleepReportBeanData?.curveListNum = cursor.getInt(cursor.getColumnIndex("curveListNum"))
                mSleepReportBeanData?.bodyMoveNum = cursor.getInt(cursor.getColumnIndex("bodyMoveNum"))
                mSleepReportBeanData?.spo2Num = cursor.getInt(cursor.getColumnIndex("spo2Num"))
                mSleepReportBeanData?.sleepCurve =
                    parseString2MutableListSleepCurveBean(cursor.getString(cursor.getColumnIndex("sleepCurve")))
                mSleepReportBeanData?.heartCurve =
                    timelessBandBleAlgorithmHelper.parseString2MutableListInt(cursor.getString(cursor.getColumnIndex("heartCurve")))
                mSleepReportBeanData?.bodyMoveCurve =
                    timelessBandBleAlgorithmHelper.parseString2MutableListInt(cursor.getString(cursor.getColumnIndex("bodyMoveCurve")))
                mSleepReportBeanData?.spo2CurveMap =
                    parseString2MutableListSpo2CurveMap(cursor.getString(cursor.getColumnIndex("spo2CurveMap")))
                mSleepReportBeanData?.score = cursor.getInt(cursor.getColumnIndex("score"))

                mSleepReportBeanData?.let { mSleepReportBeanList.add(it) }
                mSleepReportBeanData = SleepReportBean(
                    0L,0,0,0,0,0,0,
                    0,0, mutableListOf(), mutableListOf(), mutableListOf(),TreeMap<Long, Int>(),0
                )

                cursor.moveToNext() //往下一筆
            }
        }
        //將db資料放入 mSleepReportBeanList
        mSleepReportBeanData?.let {
            val list = mSleepReportBeanList.distinctBy { it.beginTime }
            mSleepReportBeanSingleEmitter?.onSuccess(list.toMutableList())
        }
    }

    @SuppressLint("Range")
    fun parseDB2SleepReport(range: DatabaseHandler.DateRange? = null) {
        val sqliteDatabase = dbHelper_sleep.writableDatabase
        val cursor = sqliteDatabase.rawQuery("select * from timelessSleep_sql", null)
        cursor.moveToFirst()
        if (cursor.count != 0) {  //count 指筆數
            for (i in 1..cursor.count) {

                val beginTime = cursor.getLong(cursor.getColumnIndex("beginTime"))
                val duration = cursor.getInt(cursor.getColumnIndex("duration"))
                val deepSleepDuration = cursor.getInt(cursor.getColumnIndex("deepSleepDuration"))
                val lightSleepDuration = cursor.getInt(cursor.getColumnIndex("lightSleepDuration"))
                val remSleepDuration = cursor.getInt(cursor.getColumnIndex("remSleepDuration"))
                val awakeSleepDuration = cursor.getInt(cursor.getColumnIndex("awakeSleepDuration"))
                val curveListNum = cursor.getInt(cursor.getColumnIndex("curveListNum"))
                val bodyMoveNum = cursor.getInt(cursor.getColumnIndex("bodyMoveNum"))
                val spo2Num = cursor.getInt(cursor.getColumnIndex("spo2Num"))
                val sleepCurve = parseString2MutableListSleepCurveBean(cursor.getString(cursor.getColumnIndex("sleepCurve")))
                val heartCurve = timelessBandBleAlgorithmHelper.parseString2MutableListInt(cursor.getString(cursor.getColumnIndex("heartCurve")))
                val bodyMoveCurve = timelessBandBleAlgorithmHelper.parseString2MutableListInt(cursor.getString(cursor.getColumnIndex("bodyMoveCurve")))
                val spo2CurveMap = parseString2MutableListSpo2CurveMap(cursor.getString(cursor.getColumnIndex("spo2CurveMap")))
                val score = cursor.getLong(cursor.getColumnIndex("score"))

                var sleepCurveList: List<SleepRecord.SleepStage> = listOf()
                var spo2List: List<Spo2Record> = listOf()
                sleepCurve.forEach({
                    sleepCurveList += SleepRecord.SleepStage(SleepRecord.SleepType.from(it.type), Date(beginTime), it.duration)
                })
                spo2CurveMap.forEach({
                    spo2List += Spo2Record(Date(it.key), it.value, Spo2Record.Spo2RecordType.AUTO)
                })
                val sleepRecord = SleepRecord(Date(beginTime), duration, deepSleepDuration, lightSleepDuration, remSleepDuration, awakeSleepDuration, sleepCurveList, spo2List, score.toInt())
                mSleepReport += sleepRecord
                cursor.moveToNext() //往下一筆
            }
        }

        mSleepReportSingleEmitter.onSuccess(mSleepReport)
    }

    private fun parseString2MutableListSleepCurveBean(string: String): MutableList<SleepCurveBean> {
        //Log.d(TAG, "SleepCurve = $string")
        val outputList = mutableListOf<SleepCurveBean>()
        val list = string.replace("[", "").replace("]","").split(", ")
        var index = 1
        var type = 0
        var duration = 0
        try {
            for (data in list) {
                if (!data.equals("")) {
                    if (index % 2 == 1) {
                        type = data.toInt()
                    } else if (index % 2 == 0) {
                        duration = data.toInt()
                        outputList.add(SleepCurveBean(type,duration))
                    }
                }
                index++
            }
        } catch (e: Exception) {
            Log.d(TAG, "格式錯誤")
        }

        return outputList
    }

    private fun parseString2MutableListSpo2CurveMap(string: String): TreeMap<Long, Int> {
        //Log.d(TAG, "Spo2CurveMap = $string")
        val outputList = TreeMap<Long, Int>()
        val list = string.replace("{", "").replace("}","").split(", ")
        for (data in list) {
            if (data.contains("=")) {
                //Log.d(TAG, "Spo2CurveMap data:  $data")
                val key = data.split("=")[0]
                val value = data.split("=")[1]
                outputList.put(key.toLong(), value.toInt())
            }
        }
        return outputList
    }



}