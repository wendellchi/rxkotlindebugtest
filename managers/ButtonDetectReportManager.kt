package com.wondercise.wondercisetimelessband.managers

import android.annotation.SuppressLint
import android.content.ContentValues
import android.util.Log
import com.android.ble.oad.Util
import com.wondercise.wondercisetimelessband.TimelessBandBleAlgorithmHelper
import com.wondercise.wondercisetimelessband.callback.ButtonDetectCallback
import com.wondercise.wondercisetimelessband.sContext
import com.wondercise.wondercisetimelessband.util.HeartRateDetect
import com.wondercise.wondercisetimelessband.util.SQliteOpenHelper_ButtonDetect
import com.wondercise.wondercisetimelessband.util.Spo2Detect

private const val TAG = "ButtonDetectReportManager"

class ButtonDetectReportManager(
    private val macAddress: String,
    private val mTimelessBandBleAlgorithmHelper: TimelessBandBleAlgorithmHelper
    )
{
    private lateinit var mButtonDetectCallback: ButtonDetectCallback
    private val mButtonHeartRateDetectDataList = mutableListOf<HeartRateDetect>()
    private val mButtonSpo2DetectDataList = mutableListOf<Spo2Detect>()
    private var mPkgConut = 0

    private val dbHelper_buttonDetect = SQliteOpenHelper_ButtonDetect(sContext, macAddress)
    private val contentValues_buttonDetect = ContentValues()

    fun reset() {
        mPkgConut = 0
        mButtonHeartRateDetectDataList.clear()
        mButtonSpo2DetectDataList.clear()
    }

    fun deleteDB(type: String) {
        //刪除db 血氧 資料
        val sqlDatabase = dbHelper_buttonDetect.writableDatabase
        sqlDatabase.delete("timelessButtonDetect_sql", "type=$type", null)
    }

    fun setCallback(call: ButtonDetectCallback) {
        mButtonDetectCallback = call
    }

    // ToDo refactoy or remove
    fun onReceivedButtonHeartRate_Spo2DetectData(data: ByteArray) {
        val type = data[0].toInt().and(0xff)
        val totalPkgNum = Util.buildUint16(data[1], data[2])
        val pkgIndex = data[3].toInt().and(0xff)

        for (pkg in 0 until pkgIndex) {
            val beginIndex = pkg * 7 + 4
            val dateTime = Util.buildUint32(data.sliceArray(beginIndex ..beginIndex + 3))

            val value = data[beginIndex + 4].toInt().and(0xff)
            val wear = data[beginIndex + 5].toInt().and(0xff)
            val skin = if (wear == 1) true else false
            val value_conf = data[beginIndex + 6].toInt().and(0xff)
            if (type == 1) {
                // mButtonHeartRateDetectDataList.add(HeartRateDetect(dateTime,skin,value,value_conf, 0,0 ))
            } else if (type == 2) {
                // mButtonSpo2DetectDataList.add(Spo2Detect(dateTime,skin,value,value_conf,0,0,0,0,0,0))
            }
        }

        mPkgConut += pkgIndex

        if (mPkgConut == totalPkgNum) {
            if (type == 1) {
                insertData2ButtonDetect_SQlite()
                parseDB2ButtonDetectBean(1)
            } else if (type == 2) {
                insertData2ButtonDetect_SQlite()
                parseDB2ButtonDetectBean(2)
            }
        }

    }


    private fun insertData2ButtonDetect_SQlite() {
        val sqlDatabase = dbHelper_buttonDetect.writableDatabase

        for (data in mButtonHeartRateDetectDataList) {
            data.let {
                contentValues_buttonDetect.put("dateTime", it.time)
                contentValues_buttonDetect.put("type", 1)
                contentValues_buttonDetect.put("value", it.hr)
                contentValues_buttonDetect.put("value_conf", it.hr_conf)
                contentValues_buttonDetect.put("wear", if(it.skin) 1 else 0)

                sqlDatabase.insert("timelessButtonDetect_sql",null, contentValues_buttonDetect)
                Log.d(TAG, "sqlDatabase insert ${it.time}, ${it.hr}, ${it.skin} OK.........")
            }
        }

        for (data in mButtonSpo2DetectDataList) {
            data.let {
                contentValues_buttonDetect.put("dateTime", it.time)
                contentValues_buttonDetect.put("type", 2)
                contentValues_buttonDetect.put("value", it.spo2)
                contentValues_buttonDetect.put("value_conf", it.spo2Conf)
                contentValues_buttonDetect.put("wear", if(it.skin) 1 else 0)

                sqlDatabase.insert("timelessButtonDetect_sql",null, contentValues_buttonDetect)
                Log.d(TAG, "sqlDatabase insert ${it.time}, ${it.spo2}, ${it.skin} OK.........")
            }
        }
        sqlDatabase.close()
    }

    @SuppressLint("Range")
    private fun parseDB2ButtonDetectBean(type: Int) {

        mButtonHeartRateDetectDataList.clear()
        mButtonSpo2DetectDataList.clear()

        val sqlDatabase = dbHelper_buttonDetect.writableDatabase
        val cursor = sqlDatabase.rawQuery("select * from timelessButtonDetect_sql", null)
        cursor.moveToFirst()
        if (cursor.count != 0) {  //count 指筆數
            for (i in 1..cursor.count) {
                val dateTime = cursor.getLong(cursor.getColumnIndex("dateTime"))
                val dbType = cursor.getInt(cursor.getColumnIndex("type"))
                val value = cursor.getInt(cursor.getColumnIndex("value"))
                val value_conf = cursor.getInt(cursor.getColumnIndex("value_conf"))
                val wear = cursor.getInt(cursor.getColumnIndex("wear"))
                val skin = if (wear == 1) true else false

                //先用心率的容器紀載手動測量資料，rri暫時當作type  1:心率  2:血氧
                if (dbType == type) {
//                    mButtonHeartRateDetectDataList.add(HeartRateDetect(dateTime, skin, value, value_conf, dbType, 0))
                }
                cursor.moveToNext() //往下一筆
            }
        }
        //將db資料放入 mDailyReportList 使用callback的方式打回去
        mButtonDetectCallback.ButtonDetectCall(mButtonHeartRateDetectDataList)
        mTimelessBandBleAlgorithmHelper.sendCommand2(byteArrayOf(0x28, 0x03))
    }


}