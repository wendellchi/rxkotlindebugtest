package com.wondercise.wondercisetimelessband.managers

import android.annotation.SuppressLint
import android.content.ContentValues
import android.util.Log
import com.android.ble.oad.Util
import com.wondercise.wondercisetimelessband.TimelessBandBleAlgorithmHelper
import com.wondercise.wondercisetimelessband.TimelessBandGlobalModule
import com.wondercise.wondercisetimelessband.database.DatabaseHandler
import com.wondercise.wondercisetimelessband.sContext
import com.wondercise.wondercisetimelessband.util.*
import io.reactivex.Single
import io.reactivex.SingleEmitter
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.util.*

private const val TAG = "HeartRateCurve24Manager"
private const val PPG_DATA_SIZE = 12

class HeartRateCurve24Manager (
    private val macAddress: String,
    private val timelessBandBleAlgorithmHelper: TimelessBandBleAlgorithmHelper
    )
{
    /**
     *
     * 一分鐘產生一筆心率數據
     *
     */
    private val dbHelper_heartRateCurve24 = SQliteOpenHelper_HeartRateCurve24(sContext, macAddress)
    private val contentValues_heartRateCurve24 = ContentValues()

    private lateinit var mHeartRateCurve24SingleEmitter: SingleEmitter<MutableList<HeartRateDetect>>
    private val mHeartRateCurve24MutableList = mutableListOf<HeartRateDetect>()

    private var mHeartRateList: List<HeartRateRecord> = listOf()
    private var mSpo2List: List<Spo2Record> = listOf()
    private var mRriList: List<HeartRateVariabilityRecord> = listOf()

    private lateinit var mHeartRateListSingleEmitter: SingleEmitter<List<HeartRateRecord>>
    private lateinit var mSpo2ListSingleEmitter: SingleEmitter<List<Spo2Record>>
    private lateinit var mRriListListSingleEmitter: SingleEmitter<List<HeartRateVariabilityRecord>>

    fun reset() {
        mHeartRateCurve24MutableList.clear()
    }

    fun heartRateResetList() {
        mHeartRateList = listOf()
    }

    fun spo2ResetList() {
        mSpo2List = listOf()
    }

    fun rriResetList() {
        mRriList = listOf()
    }

    fun deleteDB() {
        val sqliteDatabase1 = dbHelper_heartRateCurve24.writableDatabase
        sqliteDatabase1.delete("timelessHeartRateCurve24_sql", null, null)
    }

    fun setCallback(call: SingleEmitter<MutableList<HeartRateDetect>>) {
        mHeartRateCurve24SingleEmitter = call
    }

    fun setHeartRateListCallback(call: SingleEmitter<List<HeartRateRecord>>) {
        mHeartRateListSingleEmitter = call
    }

    fun setSpo2ListCallback(call: SingleEmitter<List<Spo2Record>>) {
        mSpo2ListSingleEmitter = call
    }

    fun setRriListCallback(call: SingleEmitter<List<HeartRateVariabilityRecord>>) {
        mRriListListSingleEmitter = call
    }

    fun onReceivedHeartRateCurve24(value: ByteArray) {
        val op = value[1].toInt().and(0xff)
        val expected = ByteBuffer.wrap(value.sliceArray(2..5)).order(ByteOrder.BIG_ENDIAN).int
        val received = value[6].toInt().and(0xff)
        val packageSN = value[7].toInt().and(0xff)
        Log.d(TAG, "Sync Heart Rate Records, op: $op, expected: $expected, received: $received, packageSN: $packageSN")

        when(op) {
            0x01 -> {
                for (pkg in 0 until received) {
                    val beginIndex = pkg * PPG_DATA_SIZE + 8
                    val dateTime = ByteBuffer.wrap(value.sliceArray(beginIndex..beginIndex + 3))
                        .order(ByteOrder.BIG_ENDIAN).int
                    val mode = value[beginIndex + 4].toInt()
                    val skin = (value[beginIndex + 5].toInt().and(0x01) == 0x01)
                    val hrConf = value[beginIndex + 6].toInt()
                    val rri_conf = value[beginIndex + 7].toInt()// spo2最大可信度
                    val rriBean = value[beginIndex + 8].toInt()
                    val hr = value[beginIndex + 9].toInt()
                    val rriBegin = value[beginIndex + 10]
                    val rriEnd = value[beginIndex + 11]
                    val rri = ByteBuffer.wrap(value.sliceArray(rriBegin..rriEnd)).order(ByteOrder.BIG_ENDIAN).int // 可信度最大spo2
                    when(mode) {
                        1 -> {
                            mHeartRateCurve24MutableList.add(
                                HeartRateDetect(dateTime.toLong(), mode, skin, hr, hrConf, rri_conf, rriBean, rri / 10))
                            Log.d(TAG, "HeartRateDetect dateTime: ${dateTime.toLong()}, mode: $mode, skin: $skin, hr: $hr, hrConf: $hrConf, rri_conf: $rri_conf, rriBean: $rriBean, rri: ${rri/10}")
                        }
                        4 -> {
                            mHeartRateCurve24MutableList.add(
                                HeartRateDetect(dateTime.toLong(), mode, skin, hr, hrConf, rri_conf, rriBean, rri))
                            Log.d(TAG, "HeartRateDetect dateTime: ${dateTime.toLong()}, mode: $mode, skin: $skin, hr: $hr, hrConf: $hrConf, spo2_conf: $rri_conf, rriBean: $rriBean, confSpo2: $rri")
                        }
                        else -> {
                            mHeartRateCurve24MutableList.add(
                                HeartRateDetect(dateTime.toLong(), mode,skin, hr, hrConf, 0, 0,0))
                            Log.d(TAG, "HeartRateDetect dateTime: ${dateTime.toLong()}, mode: $mode, skin: $skin, hr: $hr, hrConf: $hrConf, rri_conf: $rri_conf, rriBean: $rriBean, rri: $rri")
                        }
                    }
                }
                insertData2HeartRateCurve24_SQlite()
                // timelessBandBleAlgorithmHelper.sendCommand2(byteArrayOf(0x24, 0x02))
                mHeartRateCurve24SingleEmitter.onSuccess(mHeartRateCurve24MutableList)
            }

            0x03 -> {
                for (pkg in 0 until received) {
                    val beginIndex = pkg * PPG_DATA_SIZE + 8
                    val dateTime = ByteBuffer.wrap(value.sliceArray(beginIndex..beginIndex + 3))
                        .order(ByteOrder.BIG_ENDIAN).int
                    val mode = value[beginIndex + 4].toInt()
                    val skin = (value[beginIndex + 5].toInt().and(0x01) == 0x01)
                    val hrConf = value[beginIndex + 6].toInt()
                    val rri_conf = value[beginIndex + 7].toInt()// spo2最大可信度
                    val rriBean = value[beginIndex + 8].toInt()
                    val hr = value[beginIndex + 9].toInt()
                    val rriBegin = value[beginIndex + 10]
                    val rriEnd = value[beginIndex + 11]
                    val rri = ByteBuffer.wrap(value.sliceArray(rriBegin..rriEnd)).order(ByteOrder.BIG_ENDIAN).int // 可信度最大spo2
                    when(mode) {
                        1 -> {
                            mHeartRateCurve24MutableList.add(
                                HeartRateDetect(dateTime.toLong(), mode, skin, hr, hrConf, rri_conf, rriBean, rri / 10))
                            Log.d(TAG, "HeartRateDetect dateTime: ${dateTime.toLong()}, mode: $mode, skin: $skin, hr: $hr, hrConf: $hrConf, rri_conf: $rri_conf, rriBean: $rriBean, rri: ${rri / 10}")
                        }
                        4 -> {
                            mHeartRateCurve24MutableList.add(
                                HeartRateDetect(dateTime.toLong(), mode, skin, hr, hrConf, rri_conf, rriBean, rri / 10))
                            Log.d(TAG, "HeartRateDetect dateTime: ${dateTime.toLong()}, mode: $mode, skin: $skin, hr: $hr, hrConf: $hrConf, spo2_conf: $rri_conf, rriBean: $rriBean, confSpo2: $rri")
                        }
                        else -> {
                            mHeartRateCurve24MutableList.add(
                                HeartRateDetect(dateTime.toLong(), mode,skin, hr, hrConf, 0, 0,0))
                            Log.d(TAG, "HeartRateDetect dateTime: ${dateTime.toLong()}, mode: $mode, skin: $skin, hr: $hr, hrConf: $hrConf, rri_conf: $rri_conf, rriBean: $rriBean, rri: $rri")
                        }
                    }
                }
                insertData2HeartRateCurve24_SQlite()
                timelessBandBleAlgorithmHelper.sendCommand2(byteArrayOf(0x24, 0x04))
                mHeartRateCurve24SingleEmitter.onSuccess(mHeartRateCurve24MutableList)
            }
        }
    }

    fun onReceivedHeartRateCurve24_legacy(value: ByteArray) {
        val type = value[1].toInt().and(0xff)
        if (type == 0x00) {
            val curPkgNum = value[2].toInt().and(0xff)
            for (pkg in 0 until curPkgNum) {
                val beginIndex = pkg * 8 + 3
                val dateTime = Util.buildUint32(value.sliceArray(beginIndex ..beginIndex + 3))
                val heart = value[beginIndex + 4].toInt().and(0xff)
                val wear = value[beginIndex + 5].toInt().and(0xff)
                val skin = if (wear == 1) true else false
                val conf = value[beginIndex + 6].toInt().and(0xff)
                mHeartRateCurve24MutableList.add(HeartRateDetect(dateTime, 0, skin, heart, conf, 0, 0,0))
            }
            if (curPkgNum == 0) {
                //收到结束标志
                insertData2HeartRateCurve24_SQlite()
            } else {
                timelessBandBleAlgorithmHelper.sendCommand2(byteArrayOf(0x24, 0x00))
            }
        }
    }

    private fun insertData2HeartRateCurve24_SQlite() {
        val sqlDatabase = dbHelper_heartRateCurve24.writableDatabase
        for (data in mHeartRateCurve24MutableList) {
            data.let {
                contentValues_heartRateCurve24.put("dateTime", it.time)
                contentValues_heartRateCurve24.put("mode", it.mode)
                contentValues_heartRateCurve24.put("wear", if(it.skin) 1 else 0)
                contentValues_heartRateCurve24.put("heart", it.hr)
                contentValues_heartRateCurve24.put("conf", it.hr_conf)
                when(it.mode) {
                    1 -> {
                        contentValues_heartRateCurve24.put("rriConf", it.rri_conf)
                        contentValues_heartRateCurve24.put("rriBean", it.rriBean)
                        contentValues_heartRateCurve24.put("rri", it.rri)
                        contentValues_heartRateCurve24.put("spo2Conf", 0)
                        contentValues_heartRateCurve24.put("confSpo2", 0)
                    }
                    4 -> {
                        contentValues_heartRateCurve24.put("rriConf", 0)
                        contentValues_heartRateCurve24.put("rriBean", 0)
                        contentValues_heartRateCurve24.put("rri", 0)
                        contentValues_heartRateCurve24.put("spo2Conf", it.rri_conf)
                        contentValues_heartRateCurve24.put("confSpo2", it.rri)
                    }
                }

                sqlDatabase.insert("timelessHeartRateCurve24_sql",null, contentValues_heartRateCurve24)
                Log.d(TAG, "sqlDatabase insert ${it.time}, ${it.hr}, ${it.skin} OK.........")
            }
        }
        sqlDatabase.close()
    }

    @SuppressLint("Range")
    fun parseDB2HeartRate(range: DatabaseHandler.DateRange? = null){
        mHeartRateCurve24MutableList.clear()
        val sqlDatabase = dbHelper_heartRateCurve24.writableDatabase
        val cursor = sqlDatabase.rawQuery("select * from timelessHeartRateCurve24_sql", null)
        cursor.moveToFirst()
        if (cursor.count != 0) {  //count 指筆數
            for (i in 1..cursor.count) {
                val dateTime = cursor.getLong(cursor.getColumnIndex("dateTime"))
                val mode = cursor.getInt(cursor.getColumnIndex("mode"))
                val heart = cursor.getInt(cursor.getColumnIndex("heart"))
                val wear = cursor.getInt(cursor.getColumnIndex("wear"))
                val skin = if (wear == 1) true else false
                val conf = cursor.getInt(cursor.getColumnIndex("conf"))
                val rriConf = cursor.getInt(cursor.getColumnIndex("rriConf"))
                val rriBean = cursor.getInt(cursor.getColumnIndex("rriBean"))
                val rri = cursor.getInt(cursor.getColumnIndex("rri"))

                mHeartRateCurve24MutableList.add(HeartRateDetect(dateTime.toLong(), mode, skin, heart, conf, rriConf, rriBean, rri))

                if (TimelessBandGlobalModule.confidenceHeartRate > conf) {
                    cursor.moveToNext()
                } else {}
                if (TimelessBandGlobalModule.wornHeartRate && skin == false) {
                    cursor.moveToNext()
                } else {}

                when(mode) {
                    0,1,2 -> {
                        mHeartRateList += HeartRateRecord(Date(dateTime), heart, HeartRateRecord.HeartRateRecordType.AUTO)
                    }
                    3 -> {
                        mHeartRateList += HeartRateRecord(Date(dateTime), heart, HeartRateRecord.HeartRateRecordType.MANUAL)
                    }
                }
                cursor.moveToNext() //往下一筆
            }
        }

        //將db資料放入 mHeartRateList
        mHeartRateListSingleEmitter.onSuccess(mHeartRateList)
    }

    @SuppressLint("Range")
    fun parseDB2Spo2(range: DatabaseHandler.DateRange? = null) {
        mHeartRateCurve24MutableList.clear()
        val sqlDatabase = dbHelper_heartRateCurve24.writableDatabase
        val cursor = sqlDatabase.rawQuery("select * from timelessHeartRateCurve24_sql", null)
        cursor.moveToFirst()
        if (cursor.count != 0) {  //count 指筆數
            for (i in 1..cursor.count) {
                val dateTime = cursor.getLong(cursor.getColumnIndex("dateTime"))
                val mode = cursor.getInt(cursor.getColumnIndex("mode"))
                val heart = cursor.getInt(cursor.getColumnIndex("heart"))
                val wear = cursor.getInt(cursor.getColumnIndex("wear"))
                val skin = if (wear == 1) true else false
                val conf = cursor.getInt(cursor.getColumnIndex("conf"))
                val rriConf = cursor.getInt(cursor.getColumnIndex("rriConf"))
                val rriBean = cursor.getInt(cursor.getColumnIndex("rriBean"))
                val rri = cursor.getInt(cursor.getColumnIndex("rri"))
                val spo2Conf = cursor.getInt(cursor.getColumnIndex("spo2Conf"))
                val confSpo2 = cursor.getInt(cursor.getColumnIndex("confSpo2"))

                if (TimelessBandGlobalModule.confidenceSpo2 >= spo2Conf) {
                    cursor.moveToNext()
                    continue
                } else {}
                if (TimelessBandGlobalModule.wornSpo2 && skin == false) {
                    cursor.moveToNext()
                    continue
                } else {}

                when(mode) {
                    4 -> {
                        mSpo2List += Spo2Record(Date(dateTime), confSpo2, Spo2Record.Spo2RecordType.AUTO)
                    }
                }
                cursor.moveToNext() //往下一筆
            }
        }

        //將db資料放入 mSpo2List
        mSpo2ListSingleEmitter.onSuccess(mSpo2List)
    }

    @SuppressLint("Range")
    fun parseDB2Rri(range: DatabaseHandler.DateRange? = null) {
        mHeartRateCurve24MutableList.clear()
        val sqlDatabase = dbHelper_heartRateCurve24.writableDatabase
        val cursor = sqlDatabase.rawQuery("select * from timelessHeartRateCurve24_sql", null)
        cursor.moveToFirst()
        if (cursor.count != 0) {  //count 指筆數
            for (i in 1..cursor.count) {
                val dateTime = cursor.getLong(cursor.getColumnIndex("dateTime"))
                val mode = cursor.getInt(cursor.getColumnIndex("mode"))
                val heart = cursor.getInt(cursor.getColumnIndex("heart"))
                val wear = cursor.getInt(cursor.getColumnIndex("wear"))
                val skin = if (wear == 1) true else false
                val conf = cursor.getInt(cursor.getColumnIndex("conf"))
                val rriConf = cursor.getInt(cursor.getColumnIndex("rriConf"))
                val rriBean = cursor.getInt(cursor.getColumnIndex("rriBean"))
                val rri = cursor.getInt(cursor.getColumnIndex("rri"))
                val spo2Conf = cursor.getInt(cursor.getColumnIndex("spo2Conf"))
                val confSpo2 = cursor.getInt(cursor.getColumnIndex("confSpo2"))

                when(mode) {
                    1 -> {
                        mRriList += HeartRateVariabilityRecord(Date(dateTime), rri)
                    }
                }
                cursor.moveToNext() //往下一筆
            }
        }

        //將db資料放入 mRriList
        mRriListListSingleEmitter.onSuccess(mRriList)
    }
}