package com.wondercise.wondercisetimelessband.managers

import android.annotation.SuppressLint
import android.app.Application
import android.content.ContentValues
import android.content.Context
import android.util.Log
import androidx.core.util.rangeTo
import com.android.ble.oad.Util
import com.android.core.Utils
import com.wondercise.wondercisetimelessband.TimelessBandBleHelper
import com.wondercise.wondercisetimelessband.sContext
import com.wondercise.wondercisetimelessband.util.*
import io.reactivex.SingleEmitter
import java.lang.Error
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.util.*
import java.io.File
import com.github.doyaaaaaken.kotlincsv.dsl.csvWriter
import kotlinx.coroutines.*

private const val TAG = "HeartRatesManager"
private const val PPG_DATA_SIZE = 12
private const val END_PKG_ID = 255
private var receivedRecords = 0
private var expectedRecords = -1
private var lastPackageSN = -1
private var syncStatus = SyncStatus.IDLE

enum class SyncStatus(val code: Int, val desc: String) {
    LOST(-1, "Some packages is lost!!"),
    IDLE(0, "Idle, Waiting for Sync"),
    SYNC(1, "In Sync"),
    DONE(2, "Sync is Done")
}

private inline fun checkPackageContinuity(pkgSN: Int) : Boolean {
    var diffSN = pkgSN - lastPackageSN
    if (diffSN == 1 || diffSN == -1*END_PKG_ID) {
        lastPackageSN = pkgSN
        return true
    }
    else {
        Log.e(TAG, "pkgSN= $pkgSN, lastPackageSN= $lastPackageSN")
        return false
    }
}

private inline fun checkSyncCompleted(lastReceived: Int) :Boolean {
    receivedRecords += lastReceived
    return (expectedRecords == receivedRecords)
}
private inline fun syncEndSession(lastReceived: Int) = (lastReceived == 0)
private inline fun monitorExpected(expected: Int) = (expectedRecords != expected)

val resetSyncStatus = { receivedRecords = 0; expectedRecords = -1; syncStatus = SyncStatus.IDLE }


class HeartRatesManager (
    private val macAddress: String,
    private val timelessBandBleHelper: TimelessBandBleHelper,
    private val context: Context
    )
{
    private val roomDB by lazy { HistoryRoom.getDatabase(context) }
    private val hrdRepository by lazy { HRDRepository(roomDB.heartRateDao()) }
    private val rriRepository by lazy { RRIRepository(roomDB.rriDao()) }


    private lateinit var mHeartRatesSingleEmitter: SingleEmitter<MutableList<HeartRateData.HR>>
    private val mHRMutableList = mutableListOf<HeartRateData>()
    private val mHRVMutableList = mutableListOf<HeartRateData>()
    private val mSPO2MutableList = mutableListOf<HeartRateData>()

    fun reset() {
        resetSyncStatus()
        mHRMutableList.clear()
        mHRVMutableList.clear()
        mSPO2MutableList.clear()
    }

//    fun deleteDB() {
//        val sqliteDatabase1 = dbHelper_heartRateCurve24.writableDatabase
//        sqliteDatabase1.delete("timelessHeartRateCurve24_sql", null, null)
//    }

    fun setCallback(call: SingleEmitter<MutableList<HeartRateData.HR>>) {
        mHeartRatesSingleEmitter = call
    }


    /*
    Check after data decoded and updated
 */
    fun getSyncStatus(expected: Int, received: Int, pkgSN: Int): SyncStatus {
        Log.d(TAG, "${syncStatus.name}>> expected= $expected, received= $received, " +
                "expectedRecords= $expectedRecords, receivedRecords= $receivedRecords \n")
        val dataIsContinuous = checkPackageContinuity(pkgSN)
        val receivingIsDone = checkSyncCompleted(received)
        val syncEndSession = syncEndSession(received)
        val monitorExpected = monitorExpected(expected)
        when (syncStatus) {
            SyncStatus.IDLE -> {
                if (dataIsContinuous) {
                    expectedRecords = expected
                    receivedRecords = received
                    syncStatus = if(expectedRecords == receivedRecords) SyncStatus.DONE else SyncStatus.SYNC
                    Log.d(TAG, "${syncStatus.name}>> Sync Status is changed to $syncStatus\n")
                }
                else {
                    error("${syncStatus.name}, dataIsContineous= $dataIsContinuous, ReceivingIsDone= $receivingIsDone ")
                }
            }
            SyncStatus.SYNC -> {
                if (!dataIsContinuous) {
                    val msg = "${syncStatus.name}>> Data is not continuous. Expected SN = $lastPackageSN while getting $pkgSN \n"
                    syncStatus = SyncStatus.LOST
                    Log.e(TAG, msg)
                    error(msg)
                }
                if (receivingIsDone) {
                    syncStatus = SyncStatus.DONE
                }
            }
            SyncStatus.LOST -> {
                syncStatus = if(syncEndSession) SyncStatus.DONE else SyncStatus.LOST
            }
            SyncStatus.DONE -> {
                syncStatus = if(syncEndSession) SyncStatus.IDLE else SyncStatus.DONE
            }
            else -> {
                Log.e(TAG, "${syncStatus.name}>> Sync is stopped, Unknown Situation !!\n")
                syncStatus = SyncStatus.IDLE
            }
        }
        return syncStatus
    }

    /*
     * For ff07 characteristic, 0x24 command
     * Received on ff08, 0x24
     */
    fun onReceivedHeartRateRecords(value: ByteArray) {
        val op = value[1].toInt().and(0xff)
        val expected = ByteBuffer.wrap(value.sliceArray(2..5)).order(ByteOrder.BIG_ENDIAN).int
        val received = value[6].toInt().and(0xff)
        val packageSN = value[7].toInt().and(0xff)
        Log.d(TAG, "Sync Heart Rate Records, expected: $expected, received: $received, packageSN: $packageSN")
        if (syncStatus == SyncStatus.IDLE || syncStatus == SyncStatus.SYNC) {
            for (pkg in 0 until received) {
                val beginIndex = pkg * PPG_DATA_SIZE + 8
                heartRateDecoder(value.sliceArray(beginIndex..beginIndex + 12))?.let {
                    when (it) {
                        is HeartRateData.HR -> {
                            mHRMutableList.add(it)
                        }
                        is HeartRateData.HRV -> {
                            mHRVMutableList.add(it)
                        }
                        is HeartRateData.SPO2 -> {
                            mSPO2MutableList.add(it)
                        }
                    }
                } ?: Log.e(TAG, "Invalid Heart Rate Data")
            }
        }
        when (getSyncStatus(expected, received, packageSN)) {
            SyncStatus.LOST -> { Log.d(TAG, "Sync Status = ${syncStatus.name}") }
            SyncStatus.IDLE -> { Log.d(TAG, "Sync Status = ${syncStatus.name}") }
            SyncStatus.SYNC -> { Log.d(TAG, "Sync Status = ${syncStatus.name}") }
            SyncStatus.DONE -> {
                // ToDo Write DB and Safe to delete
                syncStatus = SyncStatus.IDLE
                when (op) {
                    0x01 -> runBlocking {
                        //获取24小时心率记录
                        launch { insertData(mHRMutableList)}
                        launch { exportHRSPO2toCSV("HRDSPO2") }
                        // timelessBandBleHelper.sendCommand2(byteArrayOf(0x24, 0x02))     // delete data
                    }
                    0x03 -> runBlocking {
                        //获取手动心率、血氧测量记录
                        launch { insertData(mHRMutableList)}
                        launch { exportHRSPO2toCSV("HRDSPO2") }

                    }
                    else -> {
                        Log.e(TAG, "ERROR unexpected opcode $op for 0x24 command\n")
                    }
                }
            }
        }
    }

    /*
     * heart rate decoder
     * parameter: 12 bytes raw data
     * return: HRrecord or SPO2record
     */
    fun heartRateDecoder(value: ByteArray) : HeartRateData? {
        val unixtime = ByteBuffer.wrap(value.sliceArray(1..4)).order(ByteOrder.BIG_ENDIAN).int
        val mode = value[5].toInt()
        val wear = (value[6].toInt().and(0x01) == 0x01)
        Log.d(TAG, "0x1A, mode= ${mode}")
        when(mode) {
            0 -> {
                val hrConf = value[7].toInt()
                val hr = value[10].toInt()
                return HeartRateData.HR(unixtime, mode, wear, hr, hrConf)
            }
            1 -> {  // RRI
                val hrConf = value[7].toInt()
                val rriConf = value[8].toInt()
                val rriInterval = value[9].toInt()
                val hr = value[10].toInt()
                val rri = ByteBuffer.wrap(value.sliceArray(11..12)).order(ByteOrder.BIG_ENDIAN).short.toInt()
                return HeartRateData.HRV(unixtime, mode, wear, hr, hrConf, rri, rriConf, rriInterval)
            }
            2 -> {
                val hrConf = value[7].toInt()
                val hr = value[10].toInt()
                return HeartRateData.HR(unixtime, mode, wear, hr, hrConf)
            }
            3 -> {
                val hrConf = value[7].toInt()
                val hr = value[10].toInt()
                return HeartRateData.HR(unixtime, mode, wear, hr, hrConf)
            }
            4 -> {  // SPO2
                val hrConf = value[7].toInt()
                val spo2Conf = value[8].toInt()
                val hr = value[10].toInt()
                val spo2 = ByteBuffer.wrap(value.sliceArray(11..12)).order(ByteOrder.BIG_ENDIAN).short.toInt()
                return HeartRateData.SPO2(unixtime, mode, wear, hr, hrConf, spo2, spo2Conf)
            }
            else -> {
                Log.e(TAG, "0x1A, mode= ${mode}")
                return null
            }
        }
    }

    suspend fun insertData(mutableList: MutableList<HeartRateData>) = withContext(Dispatchers.IO){
        mutableList.forEach {
            when(it)  {
                is HeartRateData.HR -> {
                    hrdRepository.insert(HeartRateSPO2(it.unixtime, it.mode, it.skin, it.hr, it.hr_conf, -1, -1))
                }
                is HeartRateData.HRV -> {
                    rriRepository.insert(RRI(it.unixtime, it.mode, it.skin, it.hr, it.hr_conf, it.rri, it.rri_conf, it.rri_intervals))
                }
                is HeartRateData.SPO2 -> {
                    hrdRepository.insert(HeartRateSPO2(it.unixtime, it.mode, it.skin, it.hr, it.hr_conf, it.spo2, it.spo2_conf))
                }
            }
        }
        "done"
    }


    enum class CSV_EXPORT(state: Int) {FAIL(-1), NONE(0), DONE(1)}
    fun getTimestring(ts: Long) = Utils.getFormatDateTimeStr(ts, "yyyy-MM-dd HH:mm:ss")
    fun currentTimestamp() = getTimestring(System.currentTimeMillis())
    fun getCSVFileName(tableName: String) = "${tableName}_${currentTimestamp()}.csv"

    suspend fun exportHRSPO2toCSV(tableName: String) = withContext(Dispatchers.IO) {
        val spo2History = hrdRepository.allRecords()
        val csvFile = generateFile(context, getCSVFileName(tableName))
        if (spo2History.isNotEmpty()) {
            if (csvFile != null) {
                csvWriter().open(csvFile, append = false) {
                    // Header
                    writeRow(SPO2_CSV_HEADER)
                    spo2History.forEach { it ->
                        writeRow(listOf(it.unixtime, getTimestring(it.unixtime.toLong()), it.mode, it.skin, it.hr, it.hr_conf, it.spo2, it.spo2_conf))
                    }
                }
                CSV_EXPORT.DONE
            } else {
                Log.e(TAG, "Fail to get file ${csvFile}\n")
                CSV_EXPORT.FAIL
            }
        }
        CSV_EXPORT.NONE
    }

    suspend fun exportHRRRItoCSV(tableName: String) = withContext(Dispatchers.IO) {
        val rriHistory = rriRepository.allRecords()
        val csvFile = generateFile(context, getCSVFileName(tableName))
        if (rriHistory.isNotEmpty()) {
            if (csvFile != null) {
                csvWriter().open(csvFile, append = false) {
                    // Header
                    writeRow(RRI_CSV_HEADER)
                    rriHistory.forEach { it ->
                        writeRow(listOf(it.unixtime, getTimestring(it.unixtime.toLong()), it.mode, it.skin, it.hr, it.hr_conf, it.rri, it.rri_conf, it.rri_intervals))
                    }
                }
                CSV_EXPORT.DONE
            } else {
                Log.e(TAG, "Fail to get file ${csvFile}\n")
            }
            CSV_EXPORT.FAIL
        }
        CSV_EXPORT.NONE
    }
}